import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import {App, App2} from './App';
import reportWebVitals from './reportWebVitals';
import Toggle from './components/Sate/Toggle'
import Game from './components/Tictactoe/Game';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App2 />
    <App />
    <Toggle/>
    <Game/>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
