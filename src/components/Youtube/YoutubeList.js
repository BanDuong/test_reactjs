import React from 'react';
import YoutubeData from '../../Youtube_Data.js';
import YoutubeItem from './YoutubeItem';

const YoutubeList = (props) => {
    return (
        <div className="yt-list">
            {/* {props.children} */}
            {YoutubeData.map((item, index) =>(
            <YoutubeItem key={index} id={item.id} image={item.image} title={item.title} author={item.author} avatar={item.avatar}></YoutubeItem>
            ))}
        </div>
    );
};

export default YoutubeList;