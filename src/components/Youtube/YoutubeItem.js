import React from 'react';

const YoutubeItem = (props) => {
    return (
    <div className="yt-item" id={`yt-item-${props.id}`}>
        <div className="yt-image">
            <img className="cls-img" src={props.image} alt="nulll"></img>
        </div>
        <div className="yt-footer">
            <img src={props.avatar} alt="null" className="yt-avatar"></img>
            <div className="yt-info">
            <h3 className="yt-title">{props.title}</h3>
            <h4 className="yt-author">{props.author}</h4>
            </div>
        </div>
    </div>
    );
};

export default YoutubeItem;