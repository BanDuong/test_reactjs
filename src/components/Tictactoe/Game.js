import React, { useState } from 'react';
import Board from './Board';
import  './gameStyle.css';
import calculateWinner from '../../helpers';

const Game = () => {

    const [board, setBoard] =useState(Array(9).fill(null));
    const [xIsNext, setXIsNext] = useState(true);
    const handClick = (index) =>{
        const boardCopy = [...board]; 
        if ((winner || boardCopy[index])) return;
        boardCopy[index] = xIsNext ? 'X' : 'O';
        setBoard(boardCopy);
        setXIsNext(!xIsNext);
    };
    const handleResetGame = ()=>{
        setBoard(Array(9).fill(null));
        setXIsNext(true);
    }
    const winner = calculateWinner(board);
    return (
        <div>
            <Board cells={board}
            onClick={handClick}>
            </Board>
            <button onClick={handleResetGame}>RESET</button>
        </div>
    );
};

export default Game;